# Módulo UPtogo para Magento 1.x

Este módulo integra o aplicativo UPtogo com a versão 1.x do Magento.

## Instalação

Clone o repositório ou realize o download de seus arquivos. Caso você tenha feito o download, extraia o arquivo compactado.

Acesse a pasta resultante e execute o arquivo `install.sh`, passando como parâmetro o caminho até a pasta de instalação do Magento. Por exemplo: `$ ./install.sh /var/www/magento`.

Lembre-se de informar as chaves da API e o endereço completo da loja no painel administrativo do Magento.

Caso o script de instalação não possua permissão de execução ou a pasta na qual os arquivos do módulo não estejam acessíveis ao usuário `www-data` do Apache, ocorrerão erros. Para solucioná-los, forneceça as permissões necessárias para os arquivos e pastas que compõem o módulo.

Para maior esclarecimento, veja os seguintes vídeo-tutoriais:

* [Download do Magento e dos dados de exemplo](https://uptogo.com.br/wp-content/uploads/2018/07/download-magento-e-dados-exemplo.mp4)
* [Instalação do Magento e dos dados de exemplo](https://uptogo.com.br/wp-content/uploads/2018/07/instalar-magento-e-dados-exemplo.mp4)
* [Instalação do módulo UPtogo para Magento 1.x](https://uptogo.com.br/wp-content/uploads/2018/07/instalar-modulo-uptogo-magento.mp4)

Em caso de dúvidas, acesse o site da [UPtogo](https://uptogo.com.br) ou envie um e-mail para [ti@up-fretes.com.br](mailto:ti@up-fretes.com.br).