<?php

chdir(__DIR__);
require_once('../../../../../../../vendor/autoload.php');

use \Uptogo\Sdk\Uptogo as UptogoSdk;
use \GoogleMapsHelper\GoogleMapsHelper;

class Uptogo_Magento_Model_Carrier_ShippingMethod extends Mage_Shipping_Model_Carrier_Abstract {
  
  protected $_code = 'uptogo';

  private function formatDuration(int $duration) : string {
    $duration /= 60 * 60 * 24;
    $duration = round($duration, 0);
    return $duration <= 1 ? 'Chega hoje' : "Chega em $duration dias";
  }
  
  public function collectRates(Mage_Shipping_Model_Rate_Request $request) {
    if (!Mage::getStoreConfig('carriers/' . $this->_code . '/habilitado')) {
      return false;
    }

    try {
      $googleMapsHelper = new GoogleMapsHelper(Mage::getStoreConfig('carriers/' . $this->_code . '/google_maps_api_key'));
      $origin = $googleMapsHelper->geocode($request->getDestPostcode());
      $destination = $googleMapsHelper->geocode($request->getPostcode());

      if (sizeof($origin) > 0 && sizeof($destination) > 0) {
        $route = $googleMapsHelper->directions($origin[0], $destination[0]);
        if (sizeof($route) > 0) {
          $uptogo = new UptogoSdk(Mage::getStoreConfig('carriers/' . $this->_code . '/api_key'));
          $result = Mage::getModel('shipping/rate_result');

          $expressOrder = $uptogo->calcExpress($route[0]);
          $express = Mage::getModel('shipping/rate_result_method');
          $express->setCarrier('uptogo');
          $express->setCarrierTitle('UPtogo');
          $express->setMethod('uptogo_express');
          $express->setMethodTitle(
            'Expresso - ' . $this->formatDuration(
              $expressOrder->getRoute()->getDuration()
            )
          );
          $express->setPrice($expressOrder->getPrice());
          $express->setCost($expressOrder->getPrice());

          $ecommerceOrder = $uptogo->calcEcommerce($route[0]);
          $ecommerce = Mage::getModel('shipping/rate_result_method');
          $ecommerce->setCarrier('uptogo');
          $ecommerce->setCarrierTitle('UPtogo');
          $ecommerce->setMethod('uptogo_ecommerce');
          $ecommerce->setMethodTitle(
            'E-Commerce - ' . $this->formatDuration(
              $ecommerceOrder->getRoute()->getDuration()
            )
          );
          $ecommerce->setPrice($ecommerceOrder->getPrice());
          $ecommerce->setCost($ecommerceOrder->getPrice());

          $result->append($express);
          $result->append($ecommerce);

          return $result;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } catch (Exception $e) {
      return false;
    }
  }

  public function getAllowedMethods() {
    return array($this->_code => $this->getConfigData('name'));
  }
}