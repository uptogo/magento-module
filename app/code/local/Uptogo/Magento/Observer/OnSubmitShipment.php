<?php

chdir(__DIR__);
require_once('../../../../../../vendor/autoload.php');

use \Uptogo\Sdk\Uptogo;
use \GoogleMapsHelper\GoogleMapsHelper;

class Uptogo_Magento_Observer_OnSubmitShipment {
    private function formatShippingAddress($address) {
        $result = "";
        if (sizeof($address->getStreet()) > 0) {
            foreach ($address->getStreet() as $streetIndex => $street) {
                if ($streetIndex > 0) {
                    $result .= ', ';
                }
                $result .= $street;
            }
        }
        if ($address->getPostcode() !== null) {
            $result .= ', ' . $address->getPostcode();
        }
        if ($address->getRegion() !== null) {
            $result .= ', ' . $address->getRegion();
        }
        if ($address->getCity() !== null) {
            $result .= ', ' . $address->getCity();
        }
        if ($address->getCountryId() !== null) {
            $result .= ', ' . $address->getCountryId();
        }
        return $result;
    }

    private function suspend() {
        Mage::throwException(Mage::helper('adminhtml')->__(
            'Verifique se o endereço do cliente está correto e as chaves da API no módulo UPtogo.<br>' .
            'Crie um novo pedido manualmente clicando ' .
            '<a target="_blank" href="' .
            'https://uptogo.com.br/app/usuario/logar' .
            '">aqui</a>.'
        ));
    }

    public function execute($observer) {
        $order = $observer->getEvent()->getShipment()->getOrder();
        $shippingMethod = $order->getShippingMethod();
        if (strpos($shippingMethod, 'uptogo') !== false) {
            $shippingAddress = $order->getShippingAddress();
            try {
                $googleMapsHelper = new GoogleMapsHelper(
                    Mage::getStoreConfig('carriers/uptogo/google_maps_api_key')
                );
                $origin = $googleMapsHelper->geocode(
                    Mage::getStoreConfig('shipping/origin/street_line1') . ' - ' .
                    Mage::getStoreConfig('shipping/origin/street_line2') . ' - ' .
                    Mage::getStoreConfig('shipping/origin/postcode')
                );
                if (sizeof($origin) > 0) {
                    $origin = $origin[0];
                    $destination = $googleMapsHelper->geocode(
                        $this->formatShippingAddress($shippingAddress)
                    );
                    if (sizeof($destination) > 0) {
                        $destination = $destination[0];
                        $route = $googleMapsHelper->directions($origin, $destination);
                        if (sizeof($route) > 0) {
                            $route = $route[0];
                            $uptogo = new Uptogo(Mage::getStoreConfig('carriers/uptogo/api_key'));
                            $trackPassword = 'mg_order_' . $order->getId();
                            if (strpos($shippingMethod, 'express') !== false) {
                                $uptogoOrder = $uptogo->calcExpress($route);
                                $uptogoOrder = $uptogo->newExpress($uptogoOrder, $trackPassword);
                            } else {
                                $uptogoOrder = $uptogo->calcEcommerce($route);
                                $uptogoOrder = $uptogo->newEcommerce($uptogoOrder, $trackPassword);
                            }
                            if ($uptogoOrder->getId() === null) {
                                $this->suspend();
                            } else {
                                $comment = $order->addStatusHistoryComment(
                                    'O código de rastreamento da entrega é ' .
                                    '<a target="_blank" href="' .
                                    'https://uptogo.com.br/app/pedido/rastrear/' .
                                    $uptogoOrder->getId() .
                                    '">#' . $uptogoOrder->getId() . '</a>' .
                                    ' e a senha de acesso é ' . $trackPassword . '.'
                                );
                                $comment->setIsCustomerNotified(1)->save();
                                $order->save();
                            }
                        } else {
                            $this->suspend();
                        }
                    } else {
                        $this->suspend();
                    }
                } else {
                    $this->suspend();
                }
            } catch (\Exception $e) {
                $this->suspend();
            }
        }
        return $observer;
    }
}