#!/bin/bash
if [ ! -z "$1" ]; then
    composer install
    ln -s "$PWD/app/etc/modules/Uptogo_Magento.xml" "$1/app/etc/modules/Uptogo_Magento.xml"
    if [ ! -d "$1/app/code/local" ]; then
        mkdir "$1/app/code/local"
    fi
    ln -s "$PWD/app/code/local/Uptogo" "$1/app/code/local/Uptogo"
    echo "Instalação finalizada."
    echo "Lembre-se de informar as chaves do módulo e o endereço completo no painel administrativo."
else
    echo "Informe o diretório de instalação do Magento."
fi